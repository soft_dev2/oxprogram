/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Project/Maven2/JavaApp/src/main/java/${packagePath}/${mainClassName}.java to edit this template
 */
package com.yanika.xoprogram;

import java.util.Scanner;

/**
 *
 * @author ASUS
 */
public class XOProgram {

    static char table[][] = {{'-', '-', '-'}, {'-', '-', '-'}, {'-', '-', '-'}};
    static char currentPlayer = 'O';
    static int row, col;
    static Scanner kb = new Scanner(System.in);
    static boolean  finish = false ;
    static int count = 0;

    public static void main(String[] args) {

        while (true) {
            showWelcome();
            showTable();
            showTurn();
            inputRowCol();
            Process();
            if(finish){
                break;
            }
        }

    }

    public static void showWelcome() {
        System.out.println("Welcome to OX Game");
    }

    public static void showTable() {
        for (int r = 0; r < table.length; r++) {
            for (int c = 0; c < table[r].length; c++) {
                System.out.print(table[r][c]);
            }
            System.out.println();
        }

    }

    public static void showTurn() {
        System.out.println("Turn " + currentPlayer);

    }

    public static void inputRowCol() {
        Scanner kb = new Scanner(System.in);
        System.out.println("Plese input row , col");
        row = kb.nextInt();
        col = kb.nextInt();

    }

    public static void Process() {
        if (setTable()) {
            if(checkWin()){
                finish = true ;
                showWin();
                return;
            }
            if(checkDraw()){
                finish = true;
                showTable();
                System.out.println(">>>Draw<<<");
                return;
            }
            count++;
            switcPlayer();
        }
    }

    private static void showWin() {
        showTable();
        System.out.println(">>>" + currentPlayer+" win<<<");
    }

    public static void switcPlayer() {
        if (currentPlayer == 'O') {
            currentPlayer = 'X';
        } else {
            currentPlayer = 'O';
        }

    }

    public static boolean setTable() {
        table[row - 1][col - 1] = currentPlayer;
        return true;
    }

    private static boolean checkWin() {
        if(checkVertical()){
            return true ;
        }else if(checkHorizontal()){
            return  true ;
        }else if (checkX1()){
            return true ;
        }
        
        return  false ;

    }

    public static boolean checkVertical() {
        for(int r = 0 ; r< table.length; r++){
            if(table[r][col-1]!=currentPlayer) return false ;
        }
        return true ;
    }

    public static boolean checkHorizontal() {
        for(int c = 0 ; c < table.length ; c++){
            if(table[row-1][c]!=currentPlayer) return false;
        }
        return true ;
    }
    public static  boolean  checkX(){
        if(checkX1()){
            return true ;
        }else if(checkX2()){
            return  true ;
        }
        return false ;
    }

    public static boolean checkX1() {
        for(int i = 0 ; i < table.length ; i++){
            if(table[i][i] != currentPlayer){
                return false ;
            }
        }
        return true ;
    }
    public static boolean checkX2() {
         for(int i = 0 ; i < table.length ; i++){
            if(table[i][2-i] != currentPlayer){
                return false ;
            }
        }
        return true ;
    }

    public static boolean checkDraw() {
        if (count  == 8){
            return true ;
        }
        return false;

    }
    

}
